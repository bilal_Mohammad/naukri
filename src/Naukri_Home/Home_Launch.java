package Naukri_Home;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
//comment the above line and uncomment below line to use Chrome
//import org.openqa.selenium.chrome.ChromeDriver;

public class Home_Launch {
	
public static void main(String[] args) {
	        // declaration and instantiation of objects/variables
	    	System.setProperty("webdriver.chrome.driver","C:\\Users\\v62937\\Desktop\\Escape\\Selenium java extract\\chromedriver.exe");
			WebDriver driver = new ChromeDriver();
			//comment the above 2 lines and uncomment below 2 lines to use Chrome
			//System.setProperty("webdriver.chrome.driver","G:\\chromedriver.exe");
			//WebDriver driver = new ChromeDriver();
	    	
	        String baseUrl = "https://www.naukri.com/";
	        String expectedTitle = "Recruitment - Job Search - Employment - Job Vacancies - Naukri.com";
        	        String actualTitle = "";

	        // launch Fire fox and direct it to the Base URL
	        driver.get(baseUrl);

	        // get the actual value of the title
	        actualTitle = driver.getTitle();
	        System.out.println(actualTitle);

	        /*
	         * compare the actual title of the page with the expected one and print
	         * the result as "Passed" or "Failed"
	         */
	        if (actualTitle.contains(expectedTitle)){
	            System.out.println("Test Passed!");
	        } else {
	            System.out.println("Test Failed");
	        }
	        
	        // Navigating to All child windows
	        String MainWindow=driver.getWindowHandle();		
    		
	        // To handle all new opened window.				
	            Set<String> s1=driver.getWindowHandles();	
	            System.out.println(s1);
	        Iterator<String> i1=s1.iterator();		
	        		
	        while(i1.hasNext())			
	        {		
	            String ChildWindow=i1.next();		
	            		
	            if(!MainWindow.equalsIgnoreCase(ChildWindow))			
	            {    		
	                 
	                    // Switching to Child window
	                    driver.switchTo().window(ChildWindow);	                                                                                                           
//	                    driver.findElement(By.name("emailid"))
//	                    .sendKeys("gaurav.3n@gmail.com");                			
//	                    
//	                    driver.findElement(By.name("btnLogin")).click();			
//	                                 
				// Closing the Child Window.
	                        driver.close();		
	            }		
	        }		
	        // Switching to Parent window i.e Main Window.
	            driver.switchTo().window(MainWindow);				
	
	       
	        //close Fire fox
	        driver.close();
	       
	    }
}

